const chalk = require("chalk")
const sywac = require("sywac")
const _ = require("underscore");

/**
  DESCRIPTION: Make it easy and pretty to pass command line arguments to a script
  ARGUMENTS: ({
    !name: <string>,
    !description: <string>,
    ?version: <string> « if defined, will display version of the package »,
    ?noDefaultHelp: <boolean>@default=false « if true, will not consider that passing nothing means you want to see help »,
    !config: <{
      !type: <"positional"|"boolean"|"number"|"string"|"array"|"path"|"file"|"dir">,
      !arg: <string> «
        argument syntax
        e.g. "[commandName]" "-r, --raw" "-q, --quantity <number>"
      »,
      !opts: <{
        !desc: <string> « description of the argument »,
        ?paramsDesc: [ "Command to run — available commands are: one, two, three, four, five, six, seven, eight, nine", ],
        ?defaultValue: <any>,
      }>,
    }[]> « see https://sywac.io/docs/ for full documentation »,
    ?examples: <string[]>
    ?additionalCommands: <{
      title: <string>,
      content: <[<string>, <string>][]> « ["commandName", "Command description."] »,
    }[]> « additional sections of text to display »,
    ?additionalSections: <{
      title: <string>,
      content: <string[]>,
    }[]> « additional sections of text to display »,
  })
  RETURN: <Promise.then(<{
    _: <string[]> « arguments passed »,
    [optionsNames]: <any> « value passed for this option »,
    ["h"&"help"]: <boolean> « if help was asked »,
  }
  >).catch(err)>
*/
module.exports = function (opts) {

  var clj = sywac.help("-h, --help")
    .style({

      // GLOBAL
      all: str => {
        let finalString = "\n"+ chalk.yellowBright(opts.name.toUpperCase() +":") +"\n  "+ chalk.yellow(opts.description) +"\n\n"+ str +"\n";
        return finalString.replace(/\n/g, "\n  ");
      },

      // style usage components
      usagePrefix: str => {
        return chalk.yellowBright(str.slice(0, 6)) + ' ' + chalk.magenta(str.slice(7))
      },
      usageCommandPlaceholder: str => chalk.magenta(str),
      usagePositionals: str => chalk.green(str),
      usageArgsPlaceholder: str => chalk.green(str),
      usageOptionsPlaceholder: str => chalk.green.dim(str),
      // style normal help text
      group: str => chalk.yellowBright(str),
      flags: (str, type) => {
        let style = type.datatype === 'command' ? chalk.magenta : chalk.green
        if (str.startsWith('-')) style = style.dim
        return style(str)
      },
      desc: str => chalk.cyan(str),
      hints: str => chalk.dim(str),
      example: str => {
        return chalk.yellow(str.slice(0, 2)) +
          str.slice(2).split(' ').map(word => {
            return word.startsWith('-') ? chalk.green.dim(word) : chalk.gray(word)
          }).join(' ')
      },
      // use different style when a type is invalid
      groupError: str => chalk.red(str),
      flagsError: str => chalk.red(str),
      descError: str => chalk.yellow(str),
      hintsError: str => chalk.red(str),
      // style error messages
      messages: str => chalk.red(str),
    })
  ;

  // default help
  if (!opts.noDefaultHelp) clj.showHelpByDefault();
  // version
  if (opts.version) clj.version("-v, --version", { version: opts.version, });

  // APPLY ADDITIONAL CONFIG
  _.each(opts.config, function (configObject) {
    clj[configObject.type](configObject.arg, configObject.opts);
  });

  // EXAMPLES
  if (opts.examples) _.each(opts.examples, function (example) { clj.example(example); });

  // ADDITIONAL COMMANDS AND RANDOM SECTIONS
  var epilogue = [];
  _.each(opts.additionalCommands, function (section) {
    epilogue.push(chalk.yellowBright(section.title))
    _.each(section.content, function (command) { epilogue.push("  "+ chalk.green(command[0]) +"\t"+ chalk.cyan(command[1])); });
  });
  _.each(opts.additionalSections, function (section) {
    epilogue.push(chalk.yellowBright(section.title))
    _.each(section.content, function (line) { epilogue.push("  "+ line); });
  });
  clj.epilogue(epilogue.join("\n"));

  // RETURN PROMISE
  return clj.parseAndExit();

};
