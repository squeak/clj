#!/usr/bin/env node
const _ = require("underscore");
const cly = require("clj/cly");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MANUAL
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

let manual = `
Description:
    An example command that just display some numbers.

Usage:
    example_clj <commandName> [...options]

Commands:
    one, two, three, four, five, six, seven, eight, nine

Options:
    -c|--chinese                            pass this to display chinese numbers instead
    -q=<number>|--quantity=<number>         how many time to log the value

Examples:
    # echo 9
    exampleCommand nine
    # echo 九 three times in a row
    exampleCommand nine -c -q=3
    # same in long form, and with options position changed
    exampleCommand --quantity=3 nine --chinese
    # use environment variables as well
    EXAMPLE_COMMAND_chinese=true exampleCommand five -q=6
`;

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

async function exampleCommand (commandName) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  let options = this;
  console.log(options);

  var numbers = {
    one:   [1, "一"],
    two:   [2, "二"],
    three: [3, "三"],
    four:  [4, "四"],
    five:  [5, "五"],
    six:   [6, "六"],
    seven: [7, "七"],
    eight: [8, "八"],
    nine:  [9, "九"],
  };

  if (!numbers[commandName]) return console.error("[ERROR] '"+ commandName +"' is not a valid command name")

  for (var i = 0; i < (+options.quantity || 1); i++) {
    if (options.chinese) console.log(numbers[commandName][1])
    else console.log(numbers[commandName][0]);
  };

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = exampleCommand;
if (require.main === module) cly("EXAMPLE_COMMAND", exampleCommand, manual, {
  chinese: ["-c", "--chinese"],
  quantity: ["-q=", "--quantity="],
});
