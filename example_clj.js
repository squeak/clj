#!/usr/bin/env node
const clj = require("clj");

var cljInterpreter = clj({
  name: "number",
  description: "An example command that just display some numbers.",
  config: [
    {
      type: "positional",
      arg: "[commandName]",
      opts: {
        paramsDesc: [
          "Command to run — available commands are: one, two, three, four, five, six, seven, eight, nine",
        ],
      },
    },
    {
      type: "boolean",
      arg: "-c, --chinese",
      opts: { desc: "Pass this to display chinese numbers instead" },
    },
    {
      type: "number",
      arg: "-q, --quantity <number>",
      opts: {
        desc: "How many time to log the value",
        defaultValue: 1,
      },
    },
  ],
});


cljInterpreter.then(function (argv) {

  var numbers = {
    one:   [1, "一"],
    two:   [2, "二"],
    three: [3, "三"],
    four:  [4, "四"],
    five:  [5, "五"],
    six:   [6, "六"],
    seven: [7, "七"],
    eight: [8, "八"],
    nine:  [9, "九"],
  };

  if (!numbers[argv.commandName]) return console.error("[ERROR] '"+ argv.commandName +"' is not a valid command name")

  for (var i = 0; i < argv.quantity; i++) {
    if (argv.c) console.log(numbers[argv.commandName][1])
    else console.log(numbers[argv.commandName][0]);
  }

}).catch(console.log.bind(console));
