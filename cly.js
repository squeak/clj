const _ = require("underscore");
const util = require("node:util");
const childProcess = require("node:child_process");
const execPromise = util.promisify(childProcess.exec)
const fs = require("node:fs");
const yaml = require("yaml");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MANUAL
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function manual (manualText) {
  console.log(
    manualText
      .replace(/^Description\:$/gm,   colorize("Description:", "bright.yellow",   "bold"))
      .replace(/^Usage\:$/gm,         colorize("Usage:",       "bright.cyan",     "bold"))
      .replace(/^Commands\:$/gm,      colorize("Commands:",    "bright.green",    "bold"))
      .replace(/^Options\:$/gm,       colorize("Options:",     "green",           "bold"))
      .replace(/^Examples\:$/gm,      colorize("Examples:",    "bright.magenta",  "bold"))
      .replace(/^Arguments\:$/gm,     colorize("Arguments:",   "yellow",          "bold"))
      .replace(/^Todo\:$/gm,          colorize("Todo:",        "bright.red",      "bold"))
      .replace(/^(\s*\#.*)$/gm,       colorize("$1",           "bright.black",          ))
      .replace(/^([A-Z].*\:)$/gm,     colorize("$1",           "cyan",            "bold"))
  );
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  COLOR OUTPUT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

const colors = [ "black", "red", "green", "yellow", "blue", "magenta", "cyan", "white" ];
const colorGroups = { normal: "3", background: "4", bright: "9", brightBackground: "10" };
const effects = [ "default", "bold", "dim", "italic", "underline", "blink", "blinkFast", "highlight" ];

// BUILD ALL MAPS OBJECT
const allMaps = _.mapObject(colorGroups, function (colorGroupNumber, colorGroupName) {
  return _.mapObject(_.invert(colors), function (colorNum) { return colorGroupNumber + colorNum; });
});
// add effects
_.each(effects, function (effectName, effectNumber) { allMaps[effectName] = ""+ effectNumber; });
// add normal colors shortcuts as well
_.each(colors, function (colorName, colorNumber) { allMaps[colorName] = colorGroups.normal + colorNumber; });

/**
  DESCRIPTION: colorize string for terminal
  ARGUMENTS: (
    ?string <string> « string to colorize »,
    ...colorsAndEffects <...string> « colors and effects to apply »,
  )
  RETURN: <string>
*/
function colorize (string) {
  let colorsAndEffects_codes = _.map(_.rest(arguments), function (colorOrEffect) {
    return _.get(allMaps, colorOrEffect.split("."));
  });
  return `\x1b[${colorsAndEffects_codes.join(";")}m${string}\x1b[0m`;
};

//——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//                                                  CLY
//——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

/**
  DESCRIPTION: make a node script easy to talk with from cli
  ARGUMENTS: (
    !COMMAND_ENVIRONMENT_PREFIX <string> « prefix under which to find environment variables that should be assigned as options for this command »,
    !initFunction <function> « what should be ran when command is executed in cli »,
    !manualText <string> « manual to display when "<command> -h" is ran »,
    ?flagOptions <see cly.options> «
      if you pass this, any flag passed to the command (in the format "-f" or "--flag"),
      will be added to options, and remove from list of arguments to pass to initFunction
      if you use this functionality, you can position flag options wherever you want,
      because they will be stripped before arguments are passed to the function, keeping appropriate ordering of command arguments
    »,
  )
  RETURN: <void>
*/
const cly = async function (COMMAND_ENVIRONMENT_PREFIX, initFunction, manualText, flagOptions) {

  // display manual if -h or --help was passed as any argument
  if (_.intersection(_.rest(process.argv, 2), ["-h", "--help"]).length) manual(manualText)

  // run init function (module.exports)
  else {

    // build final list of arguments to pass to initFunction
    let commandArguments = _.rest(process.argv, 2);

    // support passing options to command as environment variables in the format COMMAND_NAME_someOption
    let options = {};
    _.each(process.env, function (optionValue, ENV_VARIABLE_NAME) {
      let matcher = new RegExp(`^${COMMAND_ENVIRONMENT_PREFIX}_`);
      if (ENV_VARIABLE_NAME.match(matcher)) options[ENV_VARIABLE_NAME.replace(matcher, "")] = optionValue;
    });
    // support passing options with flags
    if (flagOptions) options = Object.assign(options, cly.options(flagOptions, commandArguments));

    // initialize
    await initFunction.apply(options, commandArguments);

  };

  // make sure that any unhandled error doesn't break everything
  // and log it for the matter, so that the little geeky debugger might have a change to understand something of what's going on
  process.on("unhandledRejection", function (err) {
    console.error(err)
    process.exit();
  });

  // make sure to exit process (don't do it otherwise unhandled rejections won't probably be detected)
  // process.exit();

};

//——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//                                                  CLY METTHODS
//——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

_.each({
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EXEC AND EXECSYNC
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: nodejs "exec" promisified
    ARGUMENTS: ( see node childProcess.exec )
    CONTEXT: <{
      logCommand: <boolean>@default=false « if true, will prettily log the command to execute before execution »,
      logResults: <boolean>@default=false « if true will log the stdout output of the command »,
    }>
    RETURN: <Promise>
  */
  exec: async function (command) {
    if (this.logCommand === true) cly.logCommand(command);
    let results = await execPromise.apply(this, arguments);
    if (this.logResults === true) console.log(results.stdout);
    return results;
  },

  /**
    DESCRIPTION: like exec, but pass the command arguments as an array
    ARGUMENTS: ( commandAsArray <string[]> )
    CONTEXT: <{
      logCommand: <boolean>@default=false « if true, will prettily log the command to execute before execution »,
      logResults: <boolean>@default=false « if true will log the stdout output of the command »,
    }>
    RETURN: <Promise>
  */
  execSpawn: function (commandAsArray) {
    var options = this;
    if (options.logCommand === true) cly.logCommand(_.toArray(commandAsArray).join(" "));
    return new Promise(function(resolve, reject) {
      let process = childProcess.spawn(commandAsArray[0], _.rest(commandAsArray));
      let stdout = "";
      let stderr = "";
      process.stdout.setEncoding("utf8");
      process.stdout.on("data", function (data) {
        if (options.logResults === true) console.log(data.toString());
        stdout += data.toString();
      });
      process.stderr.setEncoding("utf8");
      process.stderr.on("data", function (data) { stderr += data.toString(); });
      process.on("close", function (exitCode) {
        if (exitCode !== 0) reject(stderr)
        else resolve(stdout);
      });
      process.on("error", function (err) { reject(err); });
    });
  },

  /**
    DESCRIPTION:
      default nodejs "execSync" function, with utf8 default encoding,
      turning result to string stripping trailing newline character
    ARGUMENTS: (
      !command <string>,
      ?options <see nodejs execSync options>
    )
    RETURN: <string|undefined|null>
  */
  execSync: function (command, options) {

    // add utf8 encoding to options if it hasn't been otherwise customized
    if (!options) options = { encoding: "utf8" }
    else if (_.isUndefined(options.encoding)) options.encoding = "utf8";

    // run execSync
    let result = childProcess.execSync(command, options);

    // remove any eventual added newline to exec output
    return result ? result.toString().replace(/\n$/, "") : result;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  YAML
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: parse the specified yaml file
    ARGUMENTS: ( !filePath <string> « path to yaml file to parse » )
    RETURN: <any> « yaml file content »
  */
  yamlParse: function (filePath) {
    if (!filePath) return cly.colog("cly.yamlParse needs a filePath to operate", "red");
    return yaml.parse(fs.readFileSync(filePath, "utf8"), { merge: true, }); // "merge: true"  make it possible to use "<<" in yaml syntax
  },

  /**
    DESCRIPTION: stringify given value to yaml, and optionally save it to file
    ARGUMENTS: (
      !content <any> « content to parse to yaml »,
      ?filePath <string> « path where to save stringified yaml, if not defined, will simply return the stringified yaml »
    )
    RETURN: <string> « yaml content »
  */
  yamlStringify: function (content, filePath) {
    var yamlToSave = yaml.stringify(content);
    if (filePath) fs.writeFileSync(filePath, yamlToSave);
    return yamlToSave;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  LOG
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: colog, a colorfull logger
    ARGUMENTS: (
      ?string <string> « string to colorize »,
      ...colorsAndEffects <...string> « colors and effects to apply »,
    )
    RETURN: <void>
  */
  colog: function () {
    return console.log(colorize.apply(this, arguments));
  },

  /**
    DESCRIPTION: log the given command in a pretty way
    ARGUMENTS: ( command <string> )
    RETURN: <void>
  */
  logCommand: function (command) {
    console.log(`\x1b[92m❯ \x1b[35m${command}\x1b[0m`);
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  TABLE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: turn a dictionnary or pairs array into a nicely aligned keys and values string
    ARGUMENTS: (
      objectOrArray <
        | { [<string>]: <string>, }
        | [ <string>, <string>, ]
      >
    )
    RETURN: <string>
  */
  table: function (objectOrArray) {

    // make sure to have a list of [key, value] pairs
    let pairs = _.isArray(objectOrArray) ? objectOrArray : _.pairs(objectOrArray);

    // figure out longest key length
    let longestKey = _.chain(pairs).pluck(0).map("length").max().value();

    // return table
    return _.map(pairs, function (pair) {
      let spacesToAdd = "";
      for (let i = pair[0].length; i < longestKey; i++) { spacesToAdd += " "; };
      return `${pair[0]}${spacesToAdd}      ${pair[1]}`
    }).join("\n    ");

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET OPTIONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: convert flags to options
    ARGUMENTS: (
      !flagsCorrespondances <{
        [optionName]: <string[]> «
          list of flags corresponding to this option (e.g. ["-f", "--flag"])
          if flag should expect a value add an = sign at the end of the flag (e.g. ["-f=", "--flag="])
        »
      }>
      !passedFlags <string[]> « a list of flags that were passed (for example -f or --flag) »
    )
    RETURN: <object>
    EXAMPLE:
      let options = cly.options({
        chinese: ["-c", "--chinese"],
        quantity: ["-q=", "--quantity="],
      }, _.rest(arguments));
  */
  options: function (flagsCorrespondances, passedOptions) {
    let options = {};

    // iterate passedOptions (with for so that it is possible to modify passedOptions on the go, and not lose iterations)
    for (let i = 0; i < passedOptions.length; i++) {
      let passedFlag = passedOptions[i];
      if (!passedFlag.match(/^\-\-?/)) continue;

      // strip "value" if flag is in the format "--flag=value"
      let flagWithValue = passedFlag.match("=");
      let passedFlag_withoutValue = flagWithValue ? passedFlag.match(/^[^\=]*=/)[0] : passedFlag;

      // check for this flag presence in flag maps
      _.find(flagsCorrespondances, function (possibleFlags, optionName) {

        // passedFlag is not in list of matching flags
        if (_.indexOf(possibleFlags, passedFlag_withoutValue) === -1) return

        // matches (return to interrupt _.find)
        else {
          // remove flag from list of options
          passedOptions.splice(i, 1);
          // make sure to reiterate again the current index
          i--;
          // --flag=value  > options.flag = "value"
          if (flagWithValue) return options[optionName] = passedFlag.split("=")[1]
          // --flag        > options.flag = true
          else return options[optionName] = true;
        };

      });

    };

    return options;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MANUAL
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: display manual (console.log)
    ARGUMENTS: ( !manualText <string> )
    RETURN: <void>
  */
  manual: manual,

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET QUOTED PATHS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: return passed paths with quotes and escaping quotes insides paths
    ARGUMENTS: ( ...<string|string[]> )
    RETURN: <string>
    EXAMPLES:
      cly.getQuotedPaths(['/path/to/file".yop', '/other/file'], 'yetanother/file')
      // '"/path/to/file\".yop" "/other/file" "yetanother/file"'
  */
  getQuotedPaths: function () {

    // get paths from arguments (each being a path or list of paths)
    let paths = _.flatten(_.values(arguments));

    // escape quote present in paths
    let escapedPaths = _.map(paths, function (path) { return path.replace(/\"/g, "\\\""); });

    // put quote around all paths
    return `"${escapedPaths.join(`" "`)}"`;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
}, function (method, methodName) { cly[methodName] = method; });

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = cly;
