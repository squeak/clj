# clj

Just a wrapper around [sywac](https://sywac.io/), to easily create command line interface for some commands, and display pretty help pages.

Check `example_clj.js` file for an example on how to use clj.

# cly

A lighter module without dependencies (except underscorejs).
See cly.js content for documentation and `example_cly.js` file for an example on how to use cly.

Todo: add it to squeak??
